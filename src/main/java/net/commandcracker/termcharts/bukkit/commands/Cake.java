package net.commandcracker.termcharts.bukkit.commands;

import net.commandcracker.termcharts.shared.charts.Text;
import net.commandcracker.termcharts.shared.exceptions.InvalidDataException;
import net.commandcracker.termcharts.shared.objects.Data;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;

public class Cake implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        ArrayList<Data> data = new ArrayList<>();

        // Test Data
        data.add(new Data("Hydrogen", .60, ChatColor.BLUE.toString()));
        data.add(new Data("Oxygen", .25, ChatColor.WHITE.toString()));
        data.add(new Data("Carbon", .10, ChatColor.GRAY.toString()));
        data.add(new Data("Nitrogen", .02, ChatColor.BLUE.toString()));
        data.add(new Data("Other fabrics", .03, ChatColor.LIGHT_PURPLE.toString()));

        String cake, text;
        try {
            cake = net.commandcracker.termcharts.shared.charts.Cake.cake(5, 0, 0, 0, '#', data, false, false, ChatColor.RESET.toString());
            text = Text.text(data, ChatColor.RESET.toString());
            sender.sendMessage(cake);
            sender.sendMessage(text);
        } catch (InvalidDataException e) {
            sender.sendMessage(ChatColor.RED + e.getMessage() + ChatColor.RESET);
        }

        return false;
    }
}
