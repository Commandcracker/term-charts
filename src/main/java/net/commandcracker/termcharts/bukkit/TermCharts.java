package net.commandcracker.termcharts.bukkit;

import net.commandcracker.termcharts.bukkit.commands.Cake;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

public class TermCharts extends JavaPlugin implements Listener {

    @Override
    public void onEnable() {
        this.getCommand("cake").setExecutor(new Cake());
    }

}
