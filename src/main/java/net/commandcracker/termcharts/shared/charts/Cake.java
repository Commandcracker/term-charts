package net.commandcracker.termcharts.shared.charts;

import net.commandcracker.termcharts.shared.exceptions.InvalidDataException;
import net.commandcracker.termcharts.shared.objects.Data;
import net.commandcracker.termcharts.shared.objects.Span;
import net.commandcracker.termcharts.utils.ConsoleColors;

import java.util.ArrayList;
import java.util.Iterator;

public class Cake {

    public static double theta(int cx, int cy, int x, int y) {
        double angle = Math.toDegrees(Math.atan2(cy - y, x - cx)) + 90;
        angle = angle <= 180 ? angle : angle - 360;
        if (angle < 0) {
            angle = angle + 360;
        }
        return angle;
    }

    public static String cake(int radius, int xOffset, int yOffset, int whitespace, char symbol, ArrayList<Data> data, boolean middle, boolean border, String reset) throws InvalidDataException {
        // print area
        int xMin = xOffset - radius - whitespace, xMax = xOffset + radius + whitespace;
        int yMin = yOffset - radius - whitespace, yMax = yOffset + radius + whitespace;

        StringBuilder stringBuilder = new StringBuilder();

        if (!Checks.isDataValid(data)) throw new InvalidDataException("Invalid Data", new Throwable());

        // spans from Data
        ArrayList<Span> spans = new ArrayList<>();
        Iterator<Data> it = data.iterator();

        double filled = 0;
        while (it.hasNext()) {
            Data owo = it.next();
            spans.add(new Span(owo.getPercentage() + filled, filled));
            filled = owo.getPercentage() + filled;
        }

        for (int y = yMax; y >= yMin; y--) {
            for (int x = xMin; x <= xMax; x++) {
                int radiusXY = (int) Math.sqrt(Math.pow(x - xOffset, 2) + Math.pow(y - yOffset, 2));
                if (radiusXY <= radius) {
                    // whole circle
                    if (border && radiusXY == radius) {
                        // diameter
                        stringBuilder.append(ConsoleColors.RED).append(symbol).append(" ").append(reset);
                    } else {
                        if (middle && x == xOffset && y == yOffset) {
                            // center of the circle
                            stringBuilder.append(ConsoleColors.GREEN + "* ").append(reset);
                        } else {
                            // whole circle
                            double angle = theta(xOffset, yOffset, x, y);
                            boolean changed = false;

                            for (int i = 0; i < spans.size(); i++) {
                                Span span = spans.get(i);
                                if (angle < span.getA() * 360 && !(angle < span.getB() * 360)) {
                                    stringBuilder.append(data.get(i).getColor()).append(symbol).append(" ").append(reset);
                                    changed = true;
                                    break;
                                }
                            }

                            if (!changed) {
                                stringBuilder.append(ConsoleColors.BLACK_BRIGHT).append(symbol).append(" ").append(reset);
                            }

                        }
                    }
                } else {
                    // whitespace
                    stringBuilder.append("  ");
                }
            }
            // new line
            stringBuilder.append("\n");
        }
        return stringBuilder.toString();
    }
}
