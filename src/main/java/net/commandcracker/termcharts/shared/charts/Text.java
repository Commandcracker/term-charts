package net.commandcracker.termcharts.shared.charts;

import net.commandcracker.termcharts.shared.exceptions.InvalidDataException;
import net.commandcracker.termcharts.shared.objects.Data;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class Text {
    public static String text(ArrayList<Data> data, String reset) throws InvalidDataException {
        if (!Checks.isDataValid(data)) throw new InvalidDataException("Invalid Data", new Throwable());

        StringBuilder stringBuilder = new StringBuilder();

        int maxLength = 0;

        for (Data part : data) {
            int length = part.getName().length();
            if (length > maxLength) {
                maxLength = length;
            }
        }

        for (Data part : data) {
            int space = maxLength - part.getName().length();

            stringBuilder
                    .append(part.getColor())
                    .append("#")
                    .append(reset)
                    .append(" ")
                    .append(part.getName());

            for (int i = 0; i < space; i++) {
                stringBuilder.append(" ");
            }

            double percentage = part.getPercentage() * 100;

            if (percentage < 10) {
                stringBuilder.append(" ");
            }

            stringBuilder.append(" ")
                    .append(new DecimalFormat("###").format(percentage))
                    .append("%\n");
        }

        return stringBuilder.toString();
    }
}
