package net.commandcracker.termcharts.shared.charts;

import net.commandcracker.termcharts.shared.objects.Data;

import java.util.ArrayList;

public class Checks {
    public static boolean isDataValid(ArrayList<Data> data) {
        if (data.isEmpty()) return false;
        double percentage = 0;
        for (Data part : data) percentage = percentage + part.getPercentage();
        return !(percentage > 1);
    }
}
