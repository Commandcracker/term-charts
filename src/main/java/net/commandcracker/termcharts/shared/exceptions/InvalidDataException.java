package net.commandcracker.termcharts.shared.exceptions;

public class InvalidDataException extends RuntimeException {

    public InvalidDataException(String errorMessage, Throwable err) {
        super(errorMessage, err);
    }

}
