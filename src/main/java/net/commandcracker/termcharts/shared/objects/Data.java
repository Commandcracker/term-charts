package net.commandcracker.termcharts.shared.objects;

public class Data {
    final String name;
    final double percentage;
    final String color;

    public Data(String name, double percentage, String color) {
        this.name = name;
        this.percentage = percentage;
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    public double getPercentage() {
        return percentage;
    }

    public String getName() {
        return name;
    }

}
