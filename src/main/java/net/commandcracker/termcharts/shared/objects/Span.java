package net.commandcracker.termcharts.shared.objects;

public class Span {
    double a;
    double b;

    public Span(double a, double b) {
        this.a = a;
        this.b = b;
    }

    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }
}
